﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ModbusData.Repositories;

namespace vuecore.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ModController : ControllerBase
    {

        [HttpGet]
        public IActionResult Get()
        {
            var c = new HRegisterRepository();
            var r = c.GetAll();

            return Ok(r);
        }
    }
}
