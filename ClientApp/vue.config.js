const path = require('path');
module.exports = {
  outputDir: path.resolve(__dirname, "../production/dist"),
  lintOnSave: false
};
