using Microsoft.EntityFrameworkCore;
using ModbusData.Config;
using vuecoremonolito.ModbusData.Config;
using vuecoremonolito.ModbusData.Models;

namespace ModbusData
{
    public partial class MbContext:DbContext
    {
        public MbContext()
        {  
        }
        public MbContext(DbContextOptions<MbContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySql(FileConfig.Connection);
        }
        public virtual DbSet<HRegister> HRegisters { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new HRegisterConfig());
            OnModelCreatingPartial(modelBuilder);
        }
        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}