using System;
using System.Collections.Generic;
using vuecoremonolito.ModbusData.Models;
using ModbusData.Interfaces;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace ModbusData.Repositories
{
    public class HRegisterRepository
    {
        private readonly MbContext _mbContext;

        public HRegisterRepository()
        {
            _mbContext = new MbContext();
        }
        // //public async Task<HRegister> Create(HRegister hregister)
        // public  HRegister Create(HRegister hregister)
        // {
        //     if (hregister == null)
        //     {
        //         throw new ExceptionHRegister
        //         ("Entity can't be null..!!!");
        //     }

        //     try
        //     {
        //         _mbContext.HRegisters.Add(hregister);
        //         _mbContext.SaveChangesAsync();
            
        //     }
        //     catch (System.Exception e)
        //     {
        //         var m = e.Message;
        //     }
        //     return hregister;
        // }

        public dynamic GetAll()
        {
            var hregisters = _mbContext.HRegisters
            .FromSqlRaw(@"SELECT * FROM
                        (SELECT Id, Address, Value, DateCreated 
                        FROM HRegisters ORDER BY DateCreated desc limit 5) t 
                        ORDER BY t.Address ASC;")
            .ToList();

            return  hregisters;
        }

        // public IEnumerable<HRegister> GetbyAddress(string address)
        // {
        //     throw new NotImplementedException();
        // }

        // public IEnumerable<HRegister> GetbyDate(DateTime date1, DateTime date2)
        // {
        //     throw new NotImplementedException();
        // }
    }
}