using System;

namespace vuecoremonolito.ModbusData.Models
{
    public class HRegister
    {
        public Guid Id { get; set; }
        public string Address { get; set; }
        public string Value { get; set; }
        public DateTime DateCreated { get; set; }

    }
}