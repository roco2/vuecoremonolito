using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using vuecoremonolito.ModbusData.Models;

namespace ModbusData.Config
{
    public class HRegisterConfig : IEntityTypeConfiguration<HRegister>
    {
        public void Configure(EntityTypeBuilder<HRegister> builder)
        {
            builder
            .ToTable("HRegisters")
            .Property(hr => hr.Id).ValueGeneratedOnAdd();
        }
    }
}